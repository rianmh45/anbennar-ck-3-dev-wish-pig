k_devaced = {
	1000.1.1 = { change_development_level = 8 }
}

d_devaced = {
	1020.1.1 = {
		holder = 60005
	}
}

c_devaced = {
	1000.1.1 = { change_development_level = 10 }
	1020.1.1 = {
		holder = 60005
	}
}

c_godsescker = {
	1000.1.1 = { change_development_level = 10 }
	1020.1.1 = {
		holder = 60005
	}
}

c_catelsvord = {
	1000.1.1 = { change_development_level = 10 }
	1020.1.1 = {
		holder = 60001
		liege = d_vernham
	}
}

c_diremill = {
	1000.1.1 = { change_development_level = 9 }
	1020.1.1 = {
		holder = 60001
		liege = d_vernham
	}
}

d_vernham = {
	1000.1.1 = { change_development_level = 9 }
	
	1020.1.1 = {
		holder = 60000
	}
}

c_vernham = {
	1000.1.1 = { change_development_level = 12 }
	1020.1.1 = {
		holder = 60000
	}
}

c_mesenhome = {
	1020.1.1 = {
		holder = 60000
	}
}

c_oudeben = {
	1020.1.1 = {
		holder = 60001
		liege = d_vernham
	}
}

c_arrowfield = {
	1020.1.1 = {
		holder = 60000
	}
}

c_mereham = {
	1020.1.1 = {
		holder = 60000
	}
}

c_oudeben = {
	1000.1.1 = { change_development_level = 8 }
}

d_dostans_way = {
	1000.1.1 = { change_development_level = 7 }
}

c_guidesway = {
	1020.1.1 = {
		holder = 60002
	}
}

c_aldcamp = {
	1000.1.1 = { change_development_level = 5 } # It got burnt down a lil bit
	1020.1.1 = {
		holder = 60002
	}
}

c_dostans_way = {
	1000.1.1 = { change_development_level = 8 }
	1020.10.31 = {
		holder = 60003
	}
}

c_glademarch = {
	1000.1.1 = { change_development_level = 6 }
	1020.10.31 = {
		holder = 60003
	}
}