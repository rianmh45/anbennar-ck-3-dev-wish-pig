﻿namespace = anb_childhood_events

# Learn bilingual language
anb_childhood_events.0001 = {
	title = anb_childhood_events.0001.t
	desc = anb_childhood_events.0001.desc
	theme = education
	left_portrait = {
		character = root
		animation = personality_rational
	}
	background = council_chamber
	
	trigger = {
		age >= 6
		age <= 16

		can_learn_language_from_culture = yes
	}

	immediate = {
		save_scope_as = character

		# Saves the language to learn as culture_to_learn_from
		culture = {
			save_language_to_learn_from_culture = yes
		}
	}

	# Learn language
	option = {
		name = coming_of_age.0001.a
		
		add_prestige = miniscule_prestige_gain
		learn_language_of_culture = scope:culture_to_learn_from

		stress_impact = {
			bossy = minor_stress_impact_gain
			rowdy = minor_stress_impact_gain
			impatient = minor_stress_impact_gain
			lazy = minor_stress_impact_gain
			curious = minor_stress_impact_loss
			diligent = minor_stress_impact_loss
		}
		
		ai_chance = {
			base = 95
			modifier = {
				factor = 0
				OR = {
					has_trait = lazy
					has_trait = lazy
				}
			}
		}
	}
	
	# Don't learn language
	option = {
		name = coming_of_age.0001.b

		ai_chance = {
			base = 5 # Only about 20% of people don't learn language
			modifier = {
				factor = 0
				OR = {
					has_trait = diligent
					has_trait = curious
					has_trait = pensive
					has_trait = charming
				}
			}
		}
	}
}
