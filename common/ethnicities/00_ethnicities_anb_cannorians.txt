﻿@neg1_min = 0.4
@neg1_max = 0.5

@neg2_min = 0.3
@neg2_max = 0.4

@neg3_min = 0.1
@neg3_max = 0.3

@pos1_min = 0.5
@pos1_max = 0.6

@pos2_min = 0.6
@pos2_max = 0.7

@pos3_min = 0.7
@pos3_max = 0.9

@beauty1min = 0.35
@beauty1max = 0.65

@beauty2min = 0.4
@beauty2max = 0.6

@beauty3min = 0.45
@beauty3max = 0.55

@blend1min = 0.0
@blend1max = 0.2

@blend2min = 0.2
@blend2max = 0.5

@blend3min = 0.5
@blend3max = 0.8



mediterranean_vernmen = {
    visible = no
    template = "mediterranean"
    
    #More muscular in general
    gene_bs_body_shape = {
        10 = { name = body_shape_average     range = { 0.5 1.0 }      }

        15 = { name = body_shape_apple_half     range = { 0.6 0.9 }      }
        5 = { name = body_shape_apple_full     range = { 0.5 1.0 }      }

        10 = { name = body_shape_hourglass_half     range = { 0.6 0.9 }      }
        10 = { name = body_shape_hourglass_full     range = { 0.5 1.0 }      }

        10 = { name = body_shape_pear_half     range = { 0.6 0.9 }      }
        10 = { name = body_shape_pear_full     range = { 0.5 1.0 }     }

        15 = { name = body_shape_rectangle_half     range = { 0.6 0.9 }      }
        5 = { name = body_shape_rectangle_full     range = { 0.5 1.0  }    }
        15 = { name = body_shape_triangle_half     range = { 0.6 0.9 }      }
        5 = { name = body_shape_triangle_full     range = { 0.5 1.0 }      }
    }

} 

caucasian_blond_vernmen = {
    visible = no
    template = "caucasian_blond"
    
    #More muscular in general
    gene_bs_body_shape = {
        10 = { name = body_shape_average     range = { 0.5 1.0 }      }

        15 = { name = body_shape_apple_half     range = { 0.6 0.9 }      }
        5 = { name = body_shape_apple_full     range = { 0.5 1.0 }      }

        10 = { name = body_shape_hourglass_half     range = { 0.6 0.9 }      }
        10 = { name = body_shape_hourglass_full     range = { 0.5 1.0 }      }

        10 = { name = body_shape_pear_half     range = { 0.6 0.9 }      }
        10 = { name = body_shape_pear_full     range = { 0.5 1.0 }     }

        15 = { name = body_shape_rectangle_half     range = { 0.6 0.9 }      }
        5 = { name = body_shape_rectangle_full     range = { 0.5 1.0  }    }
        15 = { name = body_shape_triangle_half     range = { 0.6 0.9 }      }
        5 = { name = body_shape_triangle_full     range = { 0.5 1.0 }      }
    }



} 

caucasian_brown_hair_vernmen = {
    visible = no
    template = "caucasian_brown_hair"
    
    #More muscular in general
    gene_bs_body_shape = {
        10 = { name = body_shape_average     range = { 0.5 1.0 }      }

        15 = { name = body_shape_apple_half     range = { 0.6 0.9 }      }
        5 = { name = body_shape_apple_full     range = { 0.5 1.0 }      }

        10 = { name = body_shape_hourglass_half     range = { 0.6 0.9 }      }
        10 = { name = body_shape_hourglass_full     range = { 0.5 1.0 }      }

        10 = { name = body_shape_pear_half     range = { 0.6 0.9 }      }
        10 = { name = body_shape_pear_full     range = { 0.5 1.0 }     }

        15 = { name = body_shape_rectangle_half     range = { 0.6 0.9 }      }
        5 = { name = body_shape_rectangle_full     range = { 0.5 1.0  }    }
        15 = { name = body_shape_triangle_half     range = { 0.6 0.9 }      }
        5 = { name = body_shape_triangle_full     range = { 0.5 1.0 }      }
    }

} 

caucasian_dark_hair_vernmen = {
    visible = no
    template = "caucasian_dark_hair"
    
    #More muscular in general
    gene_bs_body_shape = {
        10 = { name = body_shape_average     range = { 0.5 1.0 }      }

        15 = { name = body_shape_apple_half     range = { 0.6 0.9 }      }
        5 = { name = body_shape_apple_full     range = { 0.5 1.0 }      }

        10 = { name = body_shape_hourglass_half     range = { 0.6 0.9 }      }
        10 = { name = body_shape_hourglass_full     range = { 0.5 1.0 }      }

        10 = { name = body_shape_pear_half     range = { 0.6 0.9 }      }
        10 = { name = body_shape_pear_full     range = { 0.5 1.0 }     }

        15 = { name = body_shape_rectangle_half     range = { 0.6 0.9 }      }
        5 = { name = body_shape_rectangle_full     range = { 0.5 1.0  }    }
        15 = { name = body_shape_triangle_half     range = { 0.6 0.9 }      }
        5 = { name = body_shape_triangle_full     range = { 0.5 1.0 }      }
    }

} 

caucasian_northern_blond_pearlsedger = {    #can probably empty these out
    visible = no
    template = "caucasian_northern_blond"

} 

caucasian_northern_ginger_pearlsedger = {
    visible = no
    template = "caucasian_northern_ginger"

} 

caucasian_northern_brown_hair_pearlsedger = {
    visible = no
    template = "caucasian_northern_brown_hair"



} 

caucasian_northern_dark_hair_pearlsedger = {
    visible = no
    template = "caucasian_northern_dark_hair"


}

milcorissian = {    #arannese
	visible = no
    template = "mediterranean"

    skin_color = {
		10 = { 0.0 0.0 0.3 0.1 }
    }
    
	eye_color = {
        # Brown
        10 = { 0.05 0.2 0.33 0.8 }
        # Green
        40 = { 0.5 0.01 0.67 0.8 }
        # Blue
        50 = { 0.67 0.01 1.0 0.8 }
	}
	hair_color = {
		# Blonde
		10 = { 0.02 0.01 0.5 0.5 }
		# Brown
		#33 = { 0.65 0.7 0.9 1.0 }
		# # Red
		# 0 = { 0.85 0.0 1.0 0.5 }
		# Black
		90 = { 0.02 0.8 0.5 0.95 }
	}
}